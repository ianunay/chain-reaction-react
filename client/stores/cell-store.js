var AppDispatcher = require('../AppDispatcher');

var EventEmitter = require('events').EventEmitter;

EventEmitter.prototype._maxListeners = 100;

var assign = require('object-assign');

var GameStore = require('./game-store');

var CHANGE_EVENT = 'change';

var CellStore = assign({}, EventEmitter.prototype, {

  grid: [],

  generateGrid: function(rows, columns) {

    var row = [],
        Grid = [];

    for (var i = 0; i < rows; i++) {
      row = [];
      for (var j = 0; j < columns; j++) {
        row.push({
          owner: '',
          count: 0,
          stability: '',
          activated: false
        });
      }
      Grid.push(row);
    }
    CellStore.grid = Grid;
  },

  isOwner: function(row, column, isActivated) {
    return (isActivated || this.grid[column][row].owner === '' || this.grid[column][row].owner == GameStore.activePlayer);
  },

  getLimit: function(row, column) {
    x = column;
    y = row;
    if((x == 0 || x == 8) && (y == 0 || y == 8)) {
      return 1;
    } else if(x == 0 || x == 8 || y == 0 || y == 8) {
      return 2;
    } else {
      return 3;
    }
  },

  getAdjacent: function(row, column) {
    var x = column,
        y = row,
        adjacentList = [];
    // right
    if(x+1 <= 8) {
      adjacentList.push({column: x+1, row: y});
    }
    // bottom
    if(y-1 >= 0) {
      adjacentList.push({column: x, row: y-1});
    }
    // left
    if(x-1 >= 0) {
      adjacentList.push({column: x-1, row: y});
    }
    // top
    if(y+1 <= 8) {
      adjacentList.push({column: x, row: y+1});
    }

    return adjacentList;
  },

  activateNeighbours: function(adjacentList) {
    adjacentList.map(function(pos){
      CellStore.grid[pos.column][pos.row].activated = true;
      CellStore.grid[pos.column][pos.row].owner = GameStore.activePlayer;
      return pos;
    });
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  handleClick: function(row, column, isActivated) {
    if (this.isOwner(row, column, isActivated)) {
      var count = this.grid[column][row].count,
          limit = this.getLimit(row, column);

      if(limit == count) {
        this.grid[column][row] = {
          owner: '',
          count: 0,
          stability: '',
          activated: false
        }
        this.activateNeighbours(this.getAdjacent(row, column));
      } else {
        this.grid[column][row] = {
          owner: GameStore.activePlayer,
          count: count + 1,
          stability: limit - count,
          activated: false
        }
      }
      CellStore.emitChange();
      GameStore.turnComplete();
    }
  },

  getCellState: function(row, column) {
    return this.grid[column][row]
  },

  _dispatchToken: AppDispatcher.register(function(payload) {

    var action = payload.action;

    if(action.actionType == "cell_click") {
      CellStore.handleClick(action.data.row, action.data.column, action.data.isActivated);
    }

    return true;

  })

});

module.exports = CellStore;

CellStore.generateGrid(9, 9);