var AppDispatcher = require('../AppDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var CHANGE_EVENT = 'change';

var GameStore = assign({}, EventEmitter.prototype, {

  activePlayer: 0,

  playerList: [],

  playerColors: ['#c0392b', '#2ecc71', '#2980b9', '#f39c12'],

  gameInterval: '',

  changeActivePlayer: function() {
    if (this.playerList.length-1 > this.activePlayer) {
      this.activePlayer++;
    } else {
      this.activePlayer = 0;
    }
  },

  turnComplete: function() {
    this.changeActivePlayer();
    clearInterval(this.gameInterval);
    this.emitChange();
  },

  getActivePlayer: function() {
    return this.playerList[this.activePlayer];
  },

  getPlayerColors: function() {
    return this.playerColors;
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  _dispatchToken: AppDispatcher.register(function(payload) {

    var action = payload.action;

    if(action.actionType == "change_page" && action.data.page == 1) {

      var players = action.data.state;
      GameStore.playerList = [];
      for(var player in players) {
        if(players[player])
          GameStore.playerList.push(players[player]);
      }
    }

    return true;

  })

});

module.exports = GameStore;
