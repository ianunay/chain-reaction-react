var gulp = require('gulp'),
    browserify = require('browserify'),
    reactify = require('reactify'),
    gulpSequence = require('gulp-sequence'),
    Promise = require('promise'),
    uglify = require('gulp-uglify'),
    fs = require('fs'),
    gulpif = require('gulp-if'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    rename = require('gulp-rename'),
    argv = require('yargs').argv,
    babelify = require("babelify");

gulp.task('clean', function(){
    var clean = require('gulp-clean');

    return gulp.src(['./clientDist']).pipe(clean())
});

gulp.task('less', function() {
    var less = require('gulp-less'),
        minifyCSS = require('gulp-minify-css');

    return gulp.src(['./styles.less'])
                .pipe(less())
                .pipe(minifyCSS())
                .pipe(gulp.dest('./clientDist/'))
});

gulp.task('copy-assets', function() {
    var files = [
        './fonts/**',
        './img/**',
        './*.html'
    ]

    return gulp.src(files, {base: './'})
               .pipe(gulp.dest('./clientDist'))
});

gulp.task('browserify', function() {

    var b = browserify({
        entries: './app.jsx',
        transform: [reactify]
    });

    return b.bundle()
            .pipe(source('./app.jsx'))
            .pipe(buffer())
            .pipe(rename('app.js'))
            .pipe(gulpif(argv.production, uglify()))
            .pipe(gulp.dest('./clientDist/'))

});


gulp.task('build', gulpSequence('clean', 'less', 'copy-assets', 'browserify'));

gulp.task('watch', ['build'], function() {
    gulp.watch('./styles.less', ['less']);
    gulp.watch(['**/*.js', '**/*.jsx'], ['browserify']);
})
