var React = require('react'),
    Header = require('./components/header.jsx'),
    Footer = require('./components/footer.jsx'),
    Game = require('./components/game.jsx');

var ReactDom = require('react-dom');

var App = React.createClass({

  render: function() {
    return (
      <div style={{'height': '100%'}}>
        <Header />
        <Game />
        <Footer />
      </div>
    );
  }

});

// var socket = require('socket.io-client').connect('http://localhost:8080');
// socket.on('news', function (data) {
//   console.log(data);
//   socket.emit('my other event', { my: 'data' });
// });

ReactDom.render(
  <App />,
  document.getElementById('app')
);
