var React = require('react');

var ChoosePlayers = require('./pages/choose-players.jsx');
var ChainReaction = require('./pages/chain-reaction.jsx');
var Winner = require('./pages/winner.jsx');

var Game = React.createClass({

  render: function() {
    return (
      <Pages>
        <ChoosePlayers />
        <ChainReaction />
        <Winner />
      </Pages>
    );
  }

});


module.exports = Game;

var Pages = React.createClass({

  getInitialState: function() {
    return {
      page: 0
    }
  },

  componentWillMount: function() {
    pageStore.addChangeListener(this._changePage);
  },

  componentWillUnMount: function() {
    pageStore.removeChangeListener(this._changePage);
  },

  _changePage: function() {
    this.setState({page: pageStore.getPage()});
  },

  render: function() {

    var child = React.cloneElement(this.props.children[this.state.page], {
      state: pageStore.getPageState()
    })

    return (
      <div style={{'height': '100%'}}>
        {child}
      </div>
    );
  }

});

var AppDispatcher = require('../AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var CHANGE_EVENT = 'change';

var pageStore = assign({}, EventEmitter.prototype, {

  page: 0,

  state: {},

  getPage: function() {
    return this.page;
  },

  getPageState: function(){
    return this.state[this.page]
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  _dispatchToken: AppDispatcher.register(function(payload) {

    var action = payload.action;

    if(action.actionType == "change_page") {

      pageStore.page = action.data.page;
      pageStore.state[pageStore.page] = action.data.state;
      pageStore.emitChange();

    }

    return true;

  })

});
