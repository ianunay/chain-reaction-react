var React = require('react');

var assign = require('object-assign');

var GAC = require('../../action_creators/GAC');

var winner = React.createClass({

  getInitialState: function() {
    return assign(this.props.state, {});
  },

  _changePage: function(){
    var data = {
      page: 0,
      state: {

      }
    }
    GAC("change_page", data);
  },

  render: function() {
    return (
      <div className="page">
        Winner is {this.state.winner}
        <a className="navBtn" onClick={this._changePage}>Restart game</a>
      </div>
    );
  }

});


module.exports = winner;