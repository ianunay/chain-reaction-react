var React = require('react');

var Grid = require('../game_elements/grid.jsx');

var Timer = require('../game_elements/side-panel.jsx');

var assign = require('object-assign');

var GAC = require('../../action_creators/GAC');

var spring = require('react-motion').spring;
var Motion = require('react-motion').Motion;

var chainReaction = React.createClass({

  getInitialState: function() {
    return assign(this.props.state, {});
  },

  _changePage: function(){
    var data = {
      page: 2,
      state: {
        winner: "Player1"
      }
    }
    GAC("change_page", data);
  },

  render: function() {
    return (
      <div style={{'height': '100%'}}>
        <div className="page">
          <Grid state={this.state} />
          <Motion defaultStyle={{x: 0}} style={{x: spring(10)}}>
            {
              (function(val){
                return <p>{val.x}</p>;
              })
            }
          </Motion>
          <a className="navBtn" onClick={this._changePage}>Goto Finish</a>
        </div>
        <div className="side-bar">
          <Timer />
        </div>
      </div>
    );
  }

});

module.exports = chainReaction;
