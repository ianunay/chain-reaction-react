var React = require('react');

var GAC = require('../../action_creators/GAC');

var GameStore = require('../../stores/game-store');

var choosePlayers = React.createClass({

  getInitialState: function() {
    return {
      playerColors: GameStore.getPlayerColors(),
      player3: 'none',
      player4: 'none'
    };
  },

  _changePage: function(){
    var data = {
      page: 1,
      state: {
        playerName1: this.refs.player1.value,
        playerName2: this.refs.player2.value,
        playerName3: (this.state.player3 == 'none') ? '' : this.refs.player3.value,
        playerName4: (this.state.player4 == 'none') ? '' : this.refs.player4.value
      }
    }
    GAC("change_page", data);
  },

  _addPlayerClick: function(){
    if (this.state.player3 == 'none') {
      this.setState({player3: 'block'});
    } else {
      this.setState({player4: 'block'});
    }
  },

  componentDidMount: function() {
    this.refs.player1.value = "Player 1";
    this.refs.player2.value = "Player 2";
    this.refs.player3.value = "Player 3";
    this.refs.player4.value = "Player 4";
  },

  render: function() {
    return (
      <div>
        <header />
        <div className="page first">
          <h2>Choose Players</h2>
          <hr/>
          <div className="form">
            <p><span className="playerDot" style={{background: this.state.playerColors[1 - 1] }}></span><input ref="player1" className="input text" /></p>
            <p><span className="playerDot" style={{background: this.state.playerColors[2 - 1] }}></span><input ref="player2" className="input text" /></p>
            <p style={{display: this.state.player3}}><span className="playerDot" style={{background: this.state.playerColors[3 - 1] }}></span><input ref="player3" className="input text" /></p>
            <p style={{display: this.state.player4}}><span className="playerDot" style={{background: this.state.playerColors[4 - 1] }}></span><input ref="player4" className="input text" /></p>
            <p><button type="button" className="input button" onClick={this._addPlayerClick} style={{display: ((this.state.player3 == 'block') && (this.state.player4 == 'block')) ? 'none' : 'block'}}>Add player</button></p>
          </div>
          <button type="button" className="input navBtn" onClick={this._changePage}>Goto Game</button>
        </div>
        <footer />
      </div>
    );
  }

});


module.exports = choosePlayers;