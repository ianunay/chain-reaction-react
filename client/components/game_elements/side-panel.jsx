var React = require('react');

var GameStore = require('../../stores/game-store');

var SidePanel = React.createClass({

  getInitialState: function() {
    var playerMarkup = GameStore.playerList.map(function(name, index){
      return (
        <div key={index} className='clearfix stacked'>
          <div className="fixedSphere" style={{'backgroundColor': GameStore.playerColors[index]}}/>
          <div style={{'float': 'left', 'paddingLeft': '5px'}}>{name}</div>
        </div>
      )
    });
    return {
      time: 30,
      timer: '',
      activePlayer: GameStore.getActivePlayer(),
      playerMarkup: playerMarkup
    }
  },

  timer: function() {
    var time = this.state.time;
    if (time == 0) {
      GameStore.turnComplete();
    } else{
      time = time - 1;
      this.setState({time: time});
    };
  },

  resetTimer: function() {
    var timer = setInterval(this.timer, 1000);
    this.state.timer = timer;
    GameStore.gameInterval = timer;
  },

  _updateState: function() {
    this.state.time = 30;
    this.resetTimer();
    this.setState({activePlayer: GameStore.getActivePlayer()});
  },

  componentWillMount: function() {
    GameStore.addChangeListener(this._updateState);
  },

  componentDidMount: function() {
    this.resetTimer();
  },

  componentWillUnmount: function() {
    clearInterval(this.state.timer);
    GameStore.removeChangeListener(this._updateState);
  },

  renderSVGTimer: function () {
    console.log( GameStore, GameStore.playerColors, GameStore.playerColors[GameStore.activePlayer]);
    return (
        <div className="timer">
          <div className="pie animated">
            <svg viewBox="0 0 32 32"  xmlns="http://www.w3.org/2000/svg" version="1.1">
              <text x='16' y='16' textAnchor="middle">{this.state.time}</text>
              <circle r="16" cx="16" cy="16" strokeDasharray="0 100" id="outer" style={{'fill': GameStore.playerColors[GameStore.activePlayer]}} key={this.state.activePlayer} />
              <circle r="14.5" cx="16" cy="16" style={{fill: 'white'}} id="middle" />
              <circle r="14" cx="16" cy="16" style={{fill: '#ddd'}} id="center" />
            </svg>
          </div>
          <div className="player-info">
            <p>{this.state.activePlayer}</p>
            <span>{this.state.time}</span>
            <small>seconds</small>
          </div>
        </div>
      )
  },
  render: function() {
    return (
      <div style={{'paddingLeft': '20px'}}>
        <div className="stacked reverse-stacked">
          <p>Time Left:</p> 
            {this.renderSVGTimer()}
          <br/>
        </div>
        <p>{this.state.activePlayer} turn</p>
        <div className="reverse-stacked tripple">
          {this.state.playerMarkup}
        </div>
      </div>
    );
  }

});


module.exports = SidePanel;