var React = require('react');

var Cell = require('./cell.jsx');

var assign = require('object-assign');

var GAC = require('../../action_creators/GAC');

var CellStore = require('../../stores/cell-store');

var spring = require('react-motion').spring;
var Motion = require('react-motion').Motion;

var Grid = React.createClass({

  getInitialState: function() {
    this.generateGrid();
    return assign(this.props.state, {
      player: "hello"
    });
  },

  gridMarkup: [],

  generateGrid: function() {
    var rowMarkup = [];

    for (var i = 0; i < 9; i++) {
      rowMarkup = [];
      for (var j = 0; j < 9; j++) {
        rowMarkup.push(
          <Cell key={i+""+j} row={j} column={i}/>
        )
      };
      this.gridMarkup.push(
        <div className="row" key={i}>
          {rowMarkup}
        </div>
      );
    };

  },

  render: function() {
    return (
      <div className="grid">
        {this.gridMarkup}
      </div>
    );
  }

});

module.exports = Grid;