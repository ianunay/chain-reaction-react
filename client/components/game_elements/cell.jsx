var React = require('react');
var PureRenderMixin = require('react-addons-pure-render-mixin');

var assign = require('object-assign');

var GAC = require('../../action_creators/GAC');

var CellStore = require('../../stores/cell-store');

var GameStore =require('../../stores/game-store');

var Cell = React.createClass({

  mixins: [PureRenderMixin],

  getInitialState: function() {

    return {
      owner: '',
      count: 0,
      stability: '',
      activated: false
    };
  },

  componentWillMount: function() {
    CellStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function() {
    CellStore.removeChangeListner(this._onChange);
  },

  _onChange: function() {
    var newState = CellStore.getCellState(this.props.row, this.props.column);
    this.setState(newState);
  },

  _onClick: function(activated) {
    var cell = {
      row: this.props.row,
      column: this.props.column,
      isActivated: activated
    };
    GAC("cell_click", cell);
  },

  componentWillUpdate: function(nextProps, nextState) {
    if(nextState.activated) {
      this._onClick(true);
    }
  },

  render: function() {
    var stability = this.state.stability;
    var atoms = [];
    for (var i = 0; i < this.state.count; i++) {
      atoms.push(
        <div key={i} className="sphere" style={{backgroundColor: GameStore.playerColors[this.state.owner]}}/>
      )
    };
    return (
      <div className="cell" onClick={this._onClick.bind(this, false)}>
        {atoms}
      </div>
    );
  }

});

module.exports = Cell;