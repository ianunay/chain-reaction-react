var AppDispatcher = require('../AppDispatcher');

var createAction = function(actionType, data) {
  AppDispatcher.handleViewAction({
    actionType: actionType,
    data: data
  });
}

module.exports = createAction;