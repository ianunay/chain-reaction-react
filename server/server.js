var express = require('express');
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(8080);

app.use(express.static(__dirname+'/clientDist/'));

app.get('/game/:id*', function(req, res, next) {
    res.json({
        id: req.param('id'),
        path: req.param(0)
    });
});

// app.get('/', function (req, res) {
//   res.sendFile(__dirname + '/clientDist/index.html');
// });

io.on('connection', function (socket) {
  socket.emit('news', { hello: 'world' });
  socket.on('my other event', function (data) {
    console.log(data);
  });
});